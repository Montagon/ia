#include <stdlib.h>

#include "anchura.h"
#include "../P1/puzle.h"

int vacio(lista* l){
	int res = 1;
	if(l->size > 0){
		res = 0;
	}
	return res;
}

int busqueda(){
	lista abiertos;
	abiertos.vector = malloc(sizeof(tEstado));
	abiertos.size = 0;
	agregar(&abiertos, estadoInicial());
	
	lista cerrados;
	cerrados.vector = malloc(sizeof(tEstado));
	cerrados.size = 0;
	
	tEstado *actual;
	//tElemento *camino = malloc(sizeof(tEstado));
	//camino[0] = abiertos.vector[0];
	int objetivo = 0;
	do{
		actual = &abiertos.vector[0];
		dispEstado(actual);
		if(testObjetivo(actual, estadoObjetivo())){
			objetivo = 1;
		}else{
			anchura(&abiertos, &cerrados);
		}

		printf("Press 'Enter' to continue: ... ");
		while ( getchar() != '\n');
	}while(!vacio(&abiertos) && !objetivo);
	if(objetivo){
		return 1;
	}else{
		return 0;
	}
}
