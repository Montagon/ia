#ifndef _ANCHURA_H_
#define _ANCHURA_H_

#include <stdio.h>

#include "../P1/puzle.h"

#ifndef _lista_
#define _lista_
   typedef struct lista {
        tElemento* vector;
        int size;
   } lista;

   //typedef tEstado tElemento;
#endif

void agregar(lista *l, tEstado *e);

void eliminarInicio(lista *l);

void anchura(lista *abiertos, lista *cerrados);

#endif
