#include <stdlib.h>

#include "anchura.h"
#include "../P1/puzle.h"

void agregar(lista *l, tEstado *e){
	tElemento *aux = malloc((l->size+1)*sizeof(tEstado));
	int i;
	
	for(i = 0; i < l->size; ++i){
		aux[i] = l->vector[i];
	}
	l->size++;
	aux[l->size-1] = *e;
	free(l->vector);
	l->vector = aux;
}

void eliminarInicio(lista *l){
	tElemento *aux = malloc((l->size-1)*sizeof(tEstado));
	int i;
	for(i = 0; i < l->size-1; ++i){
		aux[i] = l->vector[i+1];
	}
	l->size--;
	free(l->vector);
	l->vector = aux;
}

void anchura(lista *lAbiertos, lista *cerrados){
	tEstado estado_actual = lAbiertos->vector[0];
	int i;
	
	//Metemos el estado actual en cerrados
	agregar(cerrados, &estado_actual);
	
	//Lo eliminamos de la lista de abiertos.
	eliminarInicio(lAbiertos);
	
	//Probamos los distintos operadores
	for(i = 0; i < NUM_OPERADORES; ++i){
		//Si se puede aplicar, ampliamos la lista de abiertos en uno y lo insertamos.
		if(esValido(i, &estado_actual)){
			tEstado *tmp = aplicaOperador(i, &estado_actual);
			agregar(lAbiertos, tmp);
		}
	}
	for(i = 0; i < lAbiertos->size; ++i){
		dispEstado(&lAbiertos->vector[i]);
	}
}

