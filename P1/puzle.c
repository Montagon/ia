#include <stdio.h>

#include "puzle.h"


tEstado *crearEstado(int celdas[N][N]){
	tEstado* efinal = malloc(sizeof(tEstado));
	int i, j;
	
	for(i = 0; i < N; ++i){
		for(j = 0; j < N; ++j){
			efinal->celdas[i][j] = celdas[i][j];
			if(celdas[i][j] == 0){
				efinal->fila = i;
				efinal->col = j;
			}
		}
	}
	return efinal;
}

//Me imagino que será hacer un tEstado a partir del puzle inicial
tEstado *estadoInicial(){
	tEstado* efinal = malloc(sizeof(tEstado));
	efinal = crearEstado(puzle_inicial);
	return efinal;
}

tEstado *estadoObjetivo(){
	tEstado* efinal = malloc(sizeof(tEstado));
	efinal = crearEstado(puzle_final);
	return efinal;
}

int testObjetivo(tEstado *estado, tEstado *objetivo){
	int i = 0, j = 0;
	int resultado = 1;
	
	if(estado->fila != objetivo->fila && estado->col != objetivo->col){
		resultado = 0;
	}
	while(resultado && i < N){
		j = 0;
		while(resultado && j < N){
			if(estado->celdas[i][j] != objetivo->celdas[i][j]){
				resultado = 0;
			}
			++j;
		}
		++i;
	}
	return resultado;
}


int esValido(unsigned op, tEstado*s){
	int resultado = 1;
	
	switch(op){
		case 0: if(s->fila < 1){
					resultado = 0;
				}
				break;
		case 1: if(s->fila > N-1){
					resultado = 0;
				}
				break;
		case 2:	if(s->col < 1){
					resultado = 0;
				}
				break;
		case 3: if(s->col > N-1){
					resultado = 0;
				}
				break;
		default:
				resultado = 0;
				break;
	}
	return resultado;
}

tEstado *aplicaOperador(unsigned op,tEstado *s){
	tEstado* efinal = malloc(sizeof(tEstado));
	int i, j;
	
	for(i = 0; i < N; ++i){
		for(j = 0; j < N; ++j){
			efinal->celdas[i][j] = s->celdas[i][j];
		}
	}
	efinal->fila = s->fila;
	efinal->col = s->col;
	//Empezamos con la aplicación del operador
	switch(op){
		case 0: efinal->fila = efinal->fila-1; break;
		case 1: efinal->fila = efinal->fila+1; break;
		case 2: efinal->col = efinal->col-1; break;
		case 3: efinal->col = efinal->col+1; break;
	}
	efinal->celdas[s->fila][s->col] = efinal->celdas[efinal->fila][efinal->col];
	efinal->celdas[efinal->fila][efinal->col] = 0;
	return efinal;
}

void dispEstado(tEstado *s){
	int i, j;
	
	//Contenido
	for(i = 0; i < N; ++i){
		printf("%c",'|');
		for(j = 0; j < N; ++j){
			printf("%d |", s->celdas[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void dispOperador(unsigned op){
	switch(op){
		case 0: printf("%s \n", "Arriba"); break;
		case 1: printf("%s \n", "Abajo"); break;
		case 2: printf("%s \n", "Izquierda"); break;
		case 3: printf("%s \n", "Derecha"); break;
		default: break;
	}
}
