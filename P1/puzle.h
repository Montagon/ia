#ifndef _PUZLE_H_
#define _PUZLE_H_

#define ARRIBA 0
#define ABAJO 1
#define IZQUIERDA 2
#define DERECHA 3
#define NUM_OPERADORES 4
#define N 3 


#ifndef _tElemento_
#define _tElemento_
   typedef struct tEstado {
        int celdas[N][N];
        int fila, col;
   } tEstado;

   typedef tEstado tElemento;
#endif 


static int puzle_inicial[N][N] = 
{
  {1,2,3},
  {0,8,4},
  {7,6,5}
};


static int puzle_final[N][N] = 
{
  {1,2,3},
  {8,0,4},
  {7,6,5}
};


// A partir de una configuraci�n de fichas
// construye un estado v�lido para el problema
// de acuerdo al tipo de datos tEstado.
tEstado *crearEstado(int celdas[N][N]);

tEstado *estadoInicial();

tEstado *estadoObjetivo();

// Devuelve 1 si el estado s es igual al estado objetivo.
int testObjetivo(tEstado *estado, tEstado *objetivo);


// Para el puzle en situaci�n s comprueba si el movimiento op es v�lido.
// Devuelve 1 si el movimiento es v�lido y 0 en otro caso.
int esValido(unsigned op, tEstado*s);


// Para el puzle en situaci�n s aplica el operador op.
// Devuelve la nueva configuraci�n del tablero tras el movimiento.
tEstado *aplicaOperador(unsigned op,tEstado *s);


// Muestra el contenido del puzle
void dispEstado(tEstado *s);

// Muestra el nombre de cada operador: 0 Arriba, 1 Abajo, 2 Izquierda, 3 Derecha. 
void dispOperador(unsigned op);

#endif
