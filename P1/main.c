#include <stdio.h>

#include "puzle.h"

int main(){
	tEstado e = *estadoInicial();
	dispEstado(&e);
	printf("\nMovemos arriba: \n");
	tEstado e1 = *aplicaOperador(ARRIBA, &e);
	dispEstado(&e1);
	printf("\n¿Podemos mover arriba?\n");
	printf("%d\n",esValido(ARRIBA, &e1));
	printf("\nMovemos derecha: \n");
	tEstado e2 = *aplicaOperador(DERECHA, &e1);
	dispEstado(&e2);
	tEstado objetivo = *estadoObjetivo();
	printf("\n¿Es el estado objetivo?\n");
	printf("%d\n", testObjetivo(&e2, &objetivo));
	
	tEstado efinal = *aplicaOperador(DERECHA, &e);
	printf("\n¿Es el estado objetivo?\n");
	printf("Resultado final\n");
	dispEstado(&efinal);
	printf("Test objetivo\n");
	dispEstado(&objetivo);
	printf("\n%d\n", testObjetivo(&efinal, &objetivo));
	return 0;
}
